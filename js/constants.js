/***********************************************
 *                 CONSTANTES                  *
 *                                             *
 *      Aqui se guardan las variables          *
 *      requeridas por la aplicacion.          *
 *                                             *
 ***********************************************/

/** Contenedor root del proyecto. */
const ROOT_DIV = document.querySelector('#root');
/** Contenedor de la paleta de colores. */
const PALETTE_DIV = document.querySelector('#palette');
/** largo de la pantalla. */
const WIN_WIDTH = window.innerWidth;
/** largo total de la celda. */
const CELL_WIDTH = (WIN_WIDTH / CANT_CELL_WIDTH) - (CELL_BORDER * 2);
/** Alto de la pantalla. */
const WIN_HEIGHT = window.innerHeight;
/** Total de filas. */
const CANT_CELL_HEIGHT = CANT_CELL_WIDTH / 2;
/** Alto total de la celda. */
const CELL_HEIGHT = (WIN_HEIGHT / CANT_CELL_HEIGHT) - (CELL_BORDER * 2);
