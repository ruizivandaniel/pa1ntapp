/**
 * Muestro u oculta la paleta de colores dependiendo
 * de su estado actual.
 * @param force (default: '') Fuerza el display del
 * contenedor de la paleta.
 */
const togglePalette = (force = '') => {
  if (force !== '') {
    PALETTE_DIV.style.display = force;
    return;
  }
  PALETTE_DIV.style.display =
    PALETTE_DIV.style.display === 'none' ?
      'flex' :
      'none';
}

/**
 * Evento click de la celda. Cambia el color en caso
 * de que la celda actual este limpia o tenga otro color
 * diferente al actual.
 * @param div contenedor a modificar el color del fondo.
 */
const onCellClicked = (div) => {
  const index = g_div_colored.findIndex(x => x === div.id);
  if (!div.style.backgroundColor || g_isDragging || div.style.backgroundColor !== g_current_color) {
    div.style.backgroundColor = g_current_color;
    if (index === -1) {
      g_div_colored.push(div.id);
    }
  } else {
    div.style.backgroundColor = '';
    if (index !== -1) {
      g_div_colored.splice(index, 1);
    }
  }
}

/**
 * Realiza la limpieza total de la pantalla.
 */
const clear = () => {
  g_div_colored.forEach(x => {
    document.querySelector(`#${x}`).style.backgroundColor = '';
  });
  g_div_colored = [];
}

/**
 * Funcion encargada de generar la red en base a la cantidad
 * de celdas seteadas. Tambien genera la paleta de colores
 * dependiendo de los que esten seteados.
 */
const main = () => {
  // Genero la paleta de colores
  COLORS.forEach((color, index) => {
    const newColor = document.createElement('div');
    newColor.className = 'color';
    newColor.style.backgroundColor = color;
    if (index !== 0) {
      newColor.style.marginLeft = '7px';
    }
    newColor.addEventListener('click', (_) => {
      g_current_color = color;
      togglePalette('none');
    });
    PALETTE_DIV.appendChild(newColor);
  });

  // Genero el color picker.
  const pickerInput = document.createElement('input');
  pickerInput.setAttribute('type', 'color');
  pickerInput.className = 'color picker';
  pickerInput.style.marginLeft = '7px';
  pickerInput.addEventListener('change', (e) => {
    g_current_color = e.target.value;
    togglePalette('none');
  });
  PALETTE_DIV.appendChild(pickerInput);

  // Genero el boton de limpieza.
  const eraseBtn = document.createElement('div');
  eraseBtn.className = 'color erase';
  eraseBtn.style.marginLeft = '7px';
  eraseBtn.addEventListener('click', (_) => {
    togglePalette('none');
    clear();
  });
  PALETTE_DIV.appendChild(eraseBtn);

  // Genero el tablero
  for (let i = 0; i < CANT_CELL_HEIGHT; i++) {
    for (let j = 0; j < CANT_CELL_WIDTH; j++) {
      const key = Math.floor(Math.random() * (new Date().getTime()));
      const newDiv = document.createElement('div');
      newDiv.className = 'cell';
      newDiv.style.border = `${CELL_BORDER}px solid gray`;
      newDiv.style.width = `${CELL_WIDTH}px`;
      newDiv.style.height = `${CELL_HEIGHT}px`;
      newDiv.id = `c${i}${j}${key}`;
      newDiv.addEventListener('click', (_) => {
        !g_isDragging ? onCellClicked(newDiv) : null;
      });
      newDiv.addEventListener('mouseenter', (_) => {
        g_isDragging ? onCellClicked(newDiv) : null;
      });
      root.appendChild(newDiv);
    }
  }
}

// Escucho el click secundario en la pantalla.
window.addEventListener('contextmenu', (e) => {
  e.preventDefault();
  togglePalette();
});
// Escucho el inicio de arrastre del mouse.
window.addEventListener('mousedown', (e) => {
  e.preventDefault();
  g_isDragging = true;
});
// Escucho el fin de arrastre del mouse.
window.addEventListener('mouseup', (e) => {
  e.preventDefault();
  g_isDragging = false;
});
// Escucho el blur de la paleta de colores.
PALETTE_DIV.addEventListener('mouseleave', (_) => {
  togglePalette('none');
});

// main
let g_current_color = 'black';
let g_isDragging = false;
let g_div_colored = [];
PALETTE_DIV.style.display = 'none';
main();
