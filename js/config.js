/***********************************************
 *               CONFIGURACION                 *
 *                                             *
 *      Variables utiles para configurar       *
 *      la aplicacion, modifique a gusto       *
 *      para ver como se comporta la app       *
 *      dependiendo de los valores             *
 *      establecidos.                          *
 *                                             *
 ***********************************************/

/** Peso del borde de las celdas. */
const CELL_BORDER = 1;
/** Total de columnas por fila. */
const CANT_CELL_WIDTH = 60;
/** Colors. */
const COLORS = ['black', 'white', 'teal', 'blueviolet ', 'dodgerblue', 'gold', 'tomato'];
