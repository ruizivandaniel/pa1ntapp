# pa1ntapp

<br/>
![mario](./assets/mario.png)
<br/>

<br/>

## Description
Proyecto desarrollado para realizar el test tecnico para la empresa Carestino. La app fue desarrollada con javascript puro. Consta de una grilla que se crea dinamicamente en base a la cantidad de celdas configuradas y el largo total de la pantalla. Dispone de una paleta de colores que permiten colorear las grillas con el click, asi como tambien un input para seleccionar un color custom y un boton para resetear la grilla.

<br/>

## Author
Ruiz Ivan Daniel
